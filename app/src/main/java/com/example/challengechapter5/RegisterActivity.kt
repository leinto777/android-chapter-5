package com.example.challengechapter5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.challengechapter5.database.profile.Profile
import com.example.challengechapter5.database.profile.ProfileDAO
import com.example.challengechapter5.database.profile.ProfileDatabase
import com.example.challengechapter5.databinding.ActivityLoginBinding
import com.example.challengechapter5.databinding.ActivityRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)



        auth = Firebase.auth

        binding.registerButton.setOnClickListener {
            register(binding.etEmail.text.toString(), binding.etPassword.text.toString())
        }
    }

    private fun register(email: String, password: String) {
        val emailUser = binding.etEmail.text.toString()
        val alamatUser = binding.etAlamat.text.toString()
        val mobileUser = binding.etTelp.text.toString()

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    insertProfile(emailUser, alamatUser, mobileUser)
                    Toast.makeText(this, "Register Success", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(
                        baseContext,
                        "Authentication failed.",
                        Toast.LENGTH_SHORT,
                    ).show()
                }
            }
    }

    private fun insertProfile(email: String, alamat: String, mobile: String) {
         val executorService: ExecutorService = Executors.newSingleThreadExecutor()

        val profile = Profile(email = email, alamat = alamat, mobile = mobile)

        val profileDAO = ProfileDatabase.getInstance(application).profileDAO()

        executorService.execute {
            profileDAO.insert(profile)
        }

    }
}