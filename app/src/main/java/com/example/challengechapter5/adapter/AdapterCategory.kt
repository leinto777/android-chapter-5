package com.example.challengechapter5.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challengechapter5.databinding.CategoryBinding
import com.example.challengechapter5.model.DataCategoryMenu


class AdapterCategory : RecyclerView.Adapter<AdapterCategory.ViewHolder>() {

    private val diffCallBack = object : DiffUtil.ItemCallback<DataCategoryMenu>() {
        override fun areItemsTheSame(
            oldItem: DataCategoryMenu,
            newItem: DataCategoryMenu
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: DataCategoryMenu,
            newItem: DataCategoryMenu
        ): Boolean = oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallBack)

    fun submitCategoryMenuResponse(value: List<DataCategoryMenu>) = differ.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterCategory.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(CategoryBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: AdapterCategory.ViewHolder, position: Int) {
        val data = differ.currentList[position]
        data.let { holder.bind(data) }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(private var binding: CategoryBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: DataCategoryMenu) {
            binding.apply {
                categoryText.text = data.nama
                Glide.with(this.categoryImage)
                    .load(data.imageUrl)
                    .fitCenter()
                    .into(binding.categoryImage)
            }
        }
    }

}