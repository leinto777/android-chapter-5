package com.example.challengechapter5.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challengechapter5.R
import com.example.challengechapter5.databinding.GridViewBinding
import com.example.challengechapter5.databinding.ListViewBinding
import com.example.challengechapter5.model.DataListMenu

class AdapterFood (
    private val gridMode: Boolean = true,
    private val onItemClick: OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diffCallBack = object : DiffUtil.ItemCallback<DataListMenu>() {
        override fun areItemsTheSame(
            oldItem: DataListMenu,
            newItem: DataListMenu
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: DataListMenu,
            newItem: DataListMenu
        ): Boolean = oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallBack)

    fun submitListMenuResponse(value: List<DataListMenu>) = differ.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (gridMode) {
            return GridViewHolder(GridViewBinding.inflate(inflater, parent, false))
        } else {
            return ListViewHolder(ListViewBinding.inflate(inflater, parent, false))
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = differ.currentList[position]
        when (holder) {
            is GridViewHolder -> holder.bind(data)
            is ListViewHolder -> holder.bind(data)
        }

        holder.itemView.setOnClickListener {
            onItemClick.onClickItem(data)
        }
    }

    inner class GridViewHolder(private var binding: GridViewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: DataListMenu) {
            binding.apply {
                rvTitleText.text = data.nama
                rvSubTitleText.text = data.hargaFormat
                Glide.with(this.image)
                    .load(data.imageUrl)
                    .fitCenter()
                    .into(binding.image)
            }
        }
    }

    inner class ListViewHolder(private var binding: ListViewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: DataListMenu) {
            binding.apply {
                rvTitleText.text = data.nama
                rvSubTitleText.text = data.hargaFormat
                Glide.with(this.image)
                    .load(data.imageUrl)
                    .fitCenter()
                    .into(binding.image)
            }
        }
    }

    interface OnClickListener {
        fun onClickItem(data: DataListMenu)
    }
}