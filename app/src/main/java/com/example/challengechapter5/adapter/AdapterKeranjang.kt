package com.example.challengechapter5.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challengechapter5.database.keranjang.Keranjang
import com.example.challengechapter5.databinding.OrderViewBinding
import com.example.challengechapter5.viewmodel.KeranjangViewModel

class AdapterKeranjang(private val keranjangViewModel: KeranjangViewModel): RecyclerView.Adapter<AdapterKeranjang.ViewHolderClass>() {

    private var items: List<Keranjang> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderClass {
        val binding = OrderViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolderClass(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderClass, position: Int) {
        val item = items[position]
        holder.bind(item, viewModel = keranjangViewModel)
    }


    class ViewHolderClass(private val binding: OrderViewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(keranjangItem: Keranjang, viewModel: KeranjangViewModel) {
            binding.tvKeranjangOrder.text = keranjangItem.name
            binding.tvPriceOrder.text = keranjangItem.basePrice.toString()
            binding.layoutButton.totalOrderText.text = keranjangItem.quantity.toString()
            binding.viewNoteKeranjang.text= keranjangItem.note.toString()
            Glide.with(this.binding.ivKeranjangOrder)
                .load(keranjangItem.images)
                .fitCenter()
                .into(binding.ivKeranjangOrder)

            binding.deleteButton.setOnClickListener{
                viewModel.deleteItemCart(keranjangItem.id)
            }

            binding.layoutButton.plusButton.setOnClickListener {
                viewModel.increment(keranjangItem)
                binding.layoutButton.totalOrderText.text = keranjangItem.quantity.toString()
            }

            binding.layoutButton.minusButton.setOnClickListener {
                viewModel.decrement(keranjangItem)
                binding.layoutButton.totalOrderText.text = keranjangItem.quantity.toString()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(keranjangItem: List<Keranjang>) {
        this.items = keranjangItem
        notifyDataSetChanged()
    }
}