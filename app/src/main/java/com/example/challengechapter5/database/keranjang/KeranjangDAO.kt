package com.example.challengechapter5.database.keranjang

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface KeranjangDAO {
    @Insert
    fun insert(data: Keranjang)

    @Query("DELETE FROM cart")
    fun delete()

    @Update
    fun update(data: Keranjang)

    @Query("SELECT * FROM cart")
    fun getAllItems(): LiveData<List<Keranjang>>

    @Query("DELETE FROM cart WHERE id = :itemId")
    fun deleteItemById(itemId: Int): Int

    @Query("SELECT * FROM cart WHERE food_name = :foodName")
    fun getItemLive(foodName: String): Keranjang

    @Query("UPDATE cart SET food_Quantity = :newTotal WHERE food_name= :foodName")
    fun updateQuantitiyByName(newTotal: Int, foodName: String)
}
