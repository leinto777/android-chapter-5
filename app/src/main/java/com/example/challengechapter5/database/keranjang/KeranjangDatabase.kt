package com.example.challengechapter5.database.keranjang

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Keranjang::class], version = 2)
abstract class KeranjangDatabase: RoomDatabase() {

    abstract val keranjangDAO: KeranjangDAO

    companion object {

        @Volatile
        private var INSTANCE: KeranjangDatabase? = null

        fun getInstance(context: Context): KeranjangDatabase {
            synchronized(this){
                var instance = INSTANCE

                if (instance == null){
                    instance = Room.databaseBuilder(context.applicationContext,
                        KeranjangDatabase::class.java,"cart_database"
                    )
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}