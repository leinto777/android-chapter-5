package com.example.challengechapter5.helper

import com.example.challengechapter5.database.keranjang.Keranjang

interface Callback {
    fun onKeranjangLoaded(keranjang: Keranjang?): Keranjang?
}