package com.example.challengechapter5.helper

import android.content.Context
import android.content.SharedPreferences

object SharedPreferenceHelper {

    private lateinit var prefs: SharedPreferences
    private const val PREF_NAME = "layout"

    fun init(context: Context) {
        prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    fun read(key: String, value: Boolean): Boolean {
        return prefs.getBoolean(key, value)
    }

    fun write(key: String, value: Boolean) {
        val prefsEditor: SharedPreferences.Editor = prefs.edit()
        with(prefsEditor) {
            putBoolean(key, value)
            apply()
            commit()
        }
    }
}