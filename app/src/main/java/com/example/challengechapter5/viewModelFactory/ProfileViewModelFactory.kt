package com.example.challengechapter5.viewModelFactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.challengechapter5.database.profile.ProfileDAO
import com.example.challengechapter5.viewmodel.DetailViewModel
import com.example.challengechapter5.viewmodel.KeranjangViewModel
import com.example.challengechapter5.viewmodel.ProfileViewModel

@Suppress("UNCHECKED_CAST")
class ProfileViewModelFactory(private val profileDAO: ProfileDAO): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel(profileDAO) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }
}