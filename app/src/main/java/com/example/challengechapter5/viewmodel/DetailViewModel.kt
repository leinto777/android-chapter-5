package com.example.challengechapter5.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengechapter5.database.keranjang.Keranjang
import com.example.challengechapter5.database.keranjang.KeranjangDAO
import com.example.challengechapter5.database.keranjang.KeranjangDatabase
import com.example.challengechapter5.database.keranjang.KeranjangRepo
import com.example.challengechapter5.helper.Callback
import com.example.challengechapter5.model.DataListMenu

class DetailViewModel(application: Application): ViewModel() {

    private val repo: KeranjangRepo = KeranjangRepo(application)

    private val keranjangDao: KeranjangDAO

    private val _selectedItem = MutableLiveData<DataListMenu>()

    private val _counter = MutableLiveData(1)
    val counter: LiveData<Int> = _counter

//    private val _item = MutableLiveData<Keranjang>()
//    val item: LiveData<Keranjang> = _item


    init {
        val db = KeranjangDatabase.getInstance(application)
        keranjangDao = db.keranjangDAO
    }


    fun initSelectedItem(data: DataListMenu) {
        _selectedItem.value = data
    }

    fun incrementCount() {
        _counter.value = (_counter.value ?: 1) + 1
    }

    fun decrementCount() {
        val currentValue = _counter.value ?: 1
        if (currentValue > 1) {
            _counter.value = currentValue - 1
        }
    }

    private fun insertItem(keranjang: Keranjang) {
        repo.insertData(keranjang)
    }


    private fun update(keranjang: Keranjang) {
        repo.updateQuantityItem(keranjang)
    }


    fun addData(note: String) {
        val selectedItem = _selectedItem.value
        selectedItem?.let {
            val itemKeranjang = Keranjang(
                name = it.nama.toString(),
                note = note,
                basePrice = it.harga,
                totalPrice = it.harga!!.times(counter.value!!.toInt()),
                quantity = counter.value!!.toInt(),
                images = it.imageUrl.toString()
            )

            repo.getItem(itemKeranjang.name, object : Callback {
                override fun onKeranjangLoaded(keranjang: Keranjang?): Keranjang? {
                    if (keranjang != null) {
                        val total = counter.value!!.toInt() + keranjang.quantity
                        keranjang.quantity = total
                        keranjang.totalPrice = keranjang.basePrice!!.times(total)
                        // Memperbarui data dalam database
                        update(keranjang)
                    } else {
                        insertItem(itemKeranjang)
                    }
                    return keranjang
                }
            })

        }
    }

//    fun updateData(note: String) {
//        val selectedItem = _selectedItem.value
//        selectedItem?.let {
//            val itemKeranjang = Keranjang(
//                name = it.nama.toString(),
//                note = note,
//                basePrice = it.harga,
//                totalPrice = it.harga!!.times(counter.value!!.toInt()),
//                quantity = counter.value!!.toInt(),
//                images = it.imageUrl.toString()
//            )
//
//            repo.updateByName(itemKeranjang.name, object : KeranjangCallback {
//                override fun onKeranjangLoaded(keranjang: Keranjang) {
//                    val total = counter.value!!.toInt() + keranjang.quantity
//                    keranjang.quantity = total
//                    keranjang.totalPrice = keranjang.basePrice!!.times(total)
//                    // Memperbarui data dalam database
//                    update(keranjang)
//                }
//            })
//        }
//    }
}