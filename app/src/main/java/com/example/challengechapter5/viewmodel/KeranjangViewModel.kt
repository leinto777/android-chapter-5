package com.example.challengechapter5.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengechapter5.api.APIClient
import com.example.challengechapter5.database.keranjang.Keranjang
import com.example.challengechapter5.database.keranjang.KeranjangRepo
import com.example.challengechapter5.model.OrderResponse
import com.example.challengechapter5.model.OrdersData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KeranjangViewModel(application: Application): ViewModel() {
    private val repo: KeranjangRepo = KeranjangRepo(application)

    val items: LiveData<List<Keranjang>> = repo.getAllCartItems()

    private val _orderSuccess = MutableLiveData<Boolean>()
    val orderSuccess : LiveData<Boolean> = _orderSuccess


    fun deleteItemCart(itemId: Int) {
        repo.deleteItemCart(itemId)
    }

    fun deleteAllData() {
        repo.deleteAll()
    }

    private fun updateQuantity(keranjang: Keranjang) {
        repo.updateQuantityItem(keranjang)
    }

    fun increment(keranjang: Keranjang) {
        val newTotal = keranjang.quantity + 1
        keranjang.quantity = newTotal
        keranjang.totalPrice = keranjang.basePrice?.times(newTotal)

        updateQuantity(keranjang)
    }

    fun decrement(keranjang: Keranjang) {

        var newTotal = keranjang.quantity
        if (newTotal > 1) {
            newTotal = keranjang.quantity - 1
        }

        keranjang.quantity = newTotal
        keranjang.totalPrice = keranjang.basePrice?.times(newTotal)

        updateQuantity(keranjang)
    }

    fun postData(ordersData: OrdersData) {
        APIClient.instance.postOrder(ordersData)
            .enqueue(object : Callback<OrderResponse> {
                override fun onResponse(
                    call: Call<OrderResponse>,
                    response: Response<OrderResponse>
                ) {
                    val body = response.body()
                    val code = response.code()

                    if (response.isSuccessful) {
                        _orderSuccess.postValue(true)
                        deleteAllData()
                    } else {
                        _orderSuccess.postValue(false)
                    }
                }

                override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                    Log.d("ERROR", "msg: $t")
                }
            })
    }
}