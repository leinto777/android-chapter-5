package com.example.challengechapter5.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengechapter5.database.profile.Profile
import com.example.challengechapter5.database.profile.ProfileDAO
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ProfileViewModel(private val profileDAO: ProfileDAO): ViewModel() {
    var profileLiveData: LiveData<Profile> = MutableLiveData()

    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()


    fun getUserByEmail() {
        val userEmail = Firebase.auth.currentUser?.email

        profileLiveData = userEmail!!.let { profileDAO.getProfile(userEmail) }
    }

    fun updateProfileUser(profile: Profile) {
        executorService.execute { profileDAO.update(profile) }
    }

}