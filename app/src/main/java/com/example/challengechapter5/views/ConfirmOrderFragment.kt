package com.example.challengechapter5.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengechapter5.R
import com.example.challengechapter5.adapter.AdapterKeranjang
import com.example.challengechapter5.databinding.FragmentConfirmOrderBinding
import com.example.challengechapter5.databinding.FragmentDialogBinding
import com.example.challengechapter5.databinding.FragmentKeranjangBinding
import com.example.challengechapter5.model.ItemOrder
import com.example.challengechapter5.model.OrdersData
import com.example.challengechapter5.viewModelFactory.ViewModelFactory
import com.example.challengechapter5.viewmodel.KeranjangViewModel

class ConfirmOrderFragment : Fragment() {

    private var _binding: FragmentConfirmOrderBinding? = null
    private val binding get() = _binding!!

    private lateinit var keranjangViewModel: KeranjangViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConfirmOrderBinding.inflate(inflater, container, false)

        setupCartViewModel()
        showRecyclerView()
        summary()

        keranjangViewModel.orderSuccess.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(requireContext(), "Success order", Toast.LENGTH_SHORT).show()
                DialogFragment().show(childFragmentManager, DialogFragment.TAG)
            }
        }

        binding.btnCheckout.setOnClickListener {
            val username = "kevin"
            val orderItem = keranjangViewModel.items.value ?: emptyList()

            if (orderItem.isNotEmpty()) {
                //mapping
                val orderRequest = OrdersData(username, summary(), orderItem.map {
                    ItemOrder(it.name, it.quantity, it.note, it.totalPrice!!)
                })

                keranjangViewModel.postData(orderRequest)

            } else {
                Toast.makeText(requireContext(), "Data Kosong", Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }

    private fun setupCartViewModel() {
        val viewModelFactory = ViewModelFactory(requireActivity().application)
        keranjangViewModel = ViewModelProvider(this, viewModelFactory)[KeranjangViewModel::class.java]
    }

    private fun showRecyclerView() {
        val adapterKeranjang = AdapterKeranjang(keranjangViewModel)

        binding.rvConfirmOrder.adapter = adapterKeranjang
        binding.rvConfirmOrder.layoutManager = LinearLayoutManager(requireContext())

        keranjangViewModel.items.observe(viewLifecycleOwner) {
            adapterKeranjang.setData(it)

            var totalPrice = 0
            it.forEach { item ->
                totalPrice += item.totalPrice!!
            }
        }
    }

    private fun summary(): Int {
        var grandTotal = 0
        keranjangViewModel.items.observe(viewLifecycleOwner) {
            var listMenu = ""
            var totalPrice = 0
            var priceMenu = ""
            it.forEach { item ->
                listMenu += "${item.name} - ${item.quantity} x ${item.basePrice}\n"
                priceMenu += "Rp. ${item.totalPrice}\n"
                totalPrice += item.totalPrice!!
            }

            val totalText = "Rp. $totalPrice"
            grandTotal = totalPrice
            binding.tvKonfirmasiHargaPesanan.nameText.text = listMenu
            binding.tvKonfirmasiHargaPesanan.quantityText.text = priceMenu
            binding.tvKonfirmasiHargaPesanan.priceText.text = totalText
        }
        return grandTotal
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}