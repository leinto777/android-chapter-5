package com.example.challengechapter5.views

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.challengechapter5.R
import com.example.challengechapter5.databinding.FragmentDetailMenuBinding
import com.example.challengechapter5.databinding.FragmentHomeBinding
import com.example.challengechapter5.model.DataListMenu
import com.example.challengechapter5.viewModelFactory.ViewModelFactory
import com.example.challengechapter5.viewmodel.DetailViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DetailMenuFragment : Fragment() {

    private var _binding: FragmentDetailMenuBinding? = null
    private val binding get() = _binding!!

    // MutableLiveData
    private lateinit var viewModel: DetailViewModel

    private val locationUri: String = "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailMenuBinding.inflate(inflater, container, false)

        val viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory)[DetailViewModel::class.java]

        getArgs()
        openMap()
        withViewModel()
        addToKeranjang()

        return binding.root
    }

    private fun getArgs() {
        @Suppress("DEPRECATION")
        val data = arguments?.getParcelable<DataListMenu>("dataListMenu")

        binding.nameDetail.text = data?.nama
        binding.descriptionDetailFragment.text = data?.detail
        binding.priceDetail.text = data?.hargaFormat
        Glide.with(binding.imageDetail)
            .load(data?.imageUrl)
            .fitCenter()
            .into(binding.imageDetail)

        viewModel.initSelectedItem(data!!)
    }

    private fun openMap() {
        binding.detailLocation.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(locationUri))
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Google Maps tidak terinstal.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun withViewModel() {
        val observer = Observer<Int>{
            binding.orderButtons.totalOrderText.text = it.toString()
        }

        viewModel.counter.observe(viewLifecycleOwner, observer)

        binding.orderButtons.plusButton.setOnClickListener {
            viewModel.incrementCount()
        }

        binding.orderButtons.minusButton.setOnClickListener {
            viewModel.decrementCount()
        }
    }

    private fun addToKeranjang() {
        binding.buttonOrder.setOnClickListener {
            val note = binding.noteLayout.noteValue.text.toString()
            try {
                viewModel.addData(note)
                Toast.makeText(requireContext(), "Success Add Data", Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                Toast.makeText(requireContext(), "Error: $e", Toast.LENGTH_SHORT).show()
            }
        }
    }

}