package com.example.challengechapter5.views

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle

import androidx.fragment.app.DialogFragment
import com.example.challengechapter5.R

class DialogFragment : DialogFragment() {
    @SuppressLint("PrivateResource")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.order_confirmation))
            .setPositiveButton(getString(R.string.ok_order)) {_, _ -> }
            .create()

    companion object {
        const val TAG = "PurchaseConfirmationDialog"
    }

}