package com.example.challengechapter5.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.provider.ContactsContract.Data
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengechapter5.R
import com.example.challengechapter5.adapter.AdapterCategory
import com.example.challengechapter5.adapter.AdapterFood
import com.example.challengechapter5.api.APIClient
import com.example.challengechapter5.databinding.FragmentHomeBinding
import com.example.challengechapter5.helper.SharedPreferenceHelper
import com.example.challengechapter5.model.CategoryMenuResponse
import com.example.challengechapter5.model.DataListMenu
import com.example.challengechapter5.model.ListMenuResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private var isListView = true

    private val layoutIcon = arrayListOf(
        R.drawable.baseline_view_list_24,
        R.drawable.baseline_grid_view_24
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        isListView = SharedPreferenceHelper.read("isListView", true)

        val toggleButton = binding.imageButton

        toggleButton.setOnClickListener {
            isListView =  !isListView
            toggleImageViewImage(toggleButton)
            SharedPreferenceHelper.write("isListView", isListView)
            fetchListMenu()
        }

        fetchCategoryMenu()
        fetchListMenu()

        return binding.root
    }

    private fun toggleImageViewImage(imageView: ImageView) {
        imageView.setImageResource(layoutIcon[if (isListView) 0 else 1])
    }

    private fun fetchListMenu() {
        APIClient.instance.getListMenu()
            .enqueue(object : Callback<ListMenuResponse> {
                override fun onResponse(
                    call: Call<ListMenuResponse>,
                    response: Response<ListMenuResponse>
                ) {
                    val body = response.body()
                    val code = response.code()

                    if(code == 200) {
                        showLayout(body!!)
                        binding.progressBar.visibility = View.GONE
                    } else {
                        binding.progressBar.visibility = View.GONE
                    }
                }

                override fun onFailure(call: Call<ListMenuResponse>, t: Throwable) {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireActivity(), "Error: $t", Toast.LENGTH_SHORT).show()
                }
            })
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun showLayout(data: ListMenuResponse) {
        if (isListView) {
            val adapter = AdapterFood(isListView, object : AdapterFood.OnClickListener {
                override fun onClickItem(data: DataListMenu) {
                    navigateAndSendDataToDetail(data)
                }
            })
            adapter.submitListMenuResponse(data.data ?: emptyList())
            binding.rvFood.layoutManager = GridLayoutManager(requireActivity(), 2)
            binding.rvFood.adapter = adapter
        } else {
            val adapter = AdapterFood(isListView, object : AdapterFood.OnClickListener {
                override fun onClickItem(data: DataListMenu) {
                    navigateAndSendDataToDetail(data)
                }
            })

            adapter.submitListMenuResponse(data.data ?: emptyList())
            binding.rvFood.layoutManager = LinearLayoutManager(requireActivity())
            binding.rvFood.adapter = adapter
        }

    }

    private fun fetchCategoryMenu() {
        APIClient.instance.getCategoryMenu()
            .enqueue(object : Callback<CategoryMenuResponse> {
                override fun onResponse(
                    call: Call<CategoryMenuResponse>,
                    response: Response<CategoryMenuResponse>
                ) {
                    val body = response.body()
                    val code = response.code()

                    Log.d("SimpleNetworking", body.toString())
                    if(code == 200) {
                        showCategory(body!!)
                    }
                }

                override fun onFailure(call: Call<CategoryMenuResponse>, t: Throwable) {
                    Toast.makeText(requireActivity(), "Error: $t", Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun showCategory(data: CategoryMenuResponse) {
        val adapter = AdapterCategory()

        adapter.submitCategoryMenuResponse(data.data ?: emptyList())
        binding.rvCategory.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvCategory.adapter = adapter

    }

    private fun navigateAndSendDataToDetail(data: DataListMenu) {
//        val detailFragment = DetailMenuFragment()
//        val bundle = Bundle()
//        bundle.putParcelable("dataListMenu", data)
//        detailFragment.arguments = bundle
//
//        Log.d("BundleExample", bundle.toString())

        // Mengganti fragment dengan DetailMenuFragment
//        val transaction = requireActivity().supportFragmentManager.beginTransaction()
//        transaction.replace(R.id.container_fragment, detailFragment)
//        transaction.addToBackStack(null)
//        transaction.commit()

//        val action =

        val bundle = bundleOf("dataListMenu" to data)
        Log.d("BUNDLETEST", bundle.toString())
        findNavController().navigate(R.id.action_homeFragment_to_detailMenuFragment, bundle)
    }

}