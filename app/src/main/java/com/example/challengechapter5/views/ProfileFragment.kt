package com.example.challengechapter5.views

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.challengechapter5.LoginActivity
import com.example.challengechapter5.R
import com.example.challengechapter5.database.profile.ProfileDatabase
import com.example.challengechapter5.databinding.FragmentProfileBinding
import com.example.challengechapter5.viewModelFactory.ProfileViewModelFactory
import com.example.challengechapter5.viewmodel.ProfileViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth

    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        val db = ProfileDatabase.getInstance(requireContext())
        val profileDAO = db.profileDAO()
        profileViewModel = ViewModelProvider(requireActivity(), ProfileViewModelFactory(profileDAO))[ProfileViewModel::class.java]
        auth = Firebase.auth


        getProfile()
        logout()
        toEditProfile()

        return binding.root
    }

    private fun logout() {
        binding.tvLogout.setOnClickListener {
            auth.signOut()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            startActivity(intent)
        }

        binding.logoutButton.setOnClickListener {
            auth.signOut()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun getProfile() {
        profileViewModel.getUserByEmail()
        profileViewModel.profileLiveData.observe(viewLifecycleOwner){
            binding.tvEmailValue.text = it.email
            binding.tvMobileValue.text = it.mobile
            binding.tvAlamatValue.text = it.alamat
        }

    }

    private fun toEditProfile() {
        binding.btnEditProfile.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_updateProfileFragment)
        }
    }

}