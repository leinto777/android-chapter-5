package com.example.challengechapter5.views

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.challengechapter5.R
import com.example.challengechapter5.database.profile.Profile
import com.example.challengechapter5.database.profile.ProfileDatabase
import com.example.challengechapter5.databinding.FragmentUpdateProfileBinding
import com.example.challengechapter5.viewModelFactory.ProfileViewModelFactory
import com.example.challengechapter5.viewmodel.ProfileViewModel

class UpdateProfileFragment : Fragment() {

    private var _binding: FragmentUpdateProfileBinding? = null
    private val binding get() = _binding!!

    private lateinit var profile: Profile

    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpdateProfileBinding.inflate(inflater, container, false)
        val db = ProfileDatabase.getInstance(requireContext())
        val profileDAO = db.profileDAO()
        profileViewModel = ViewModelProvider(requireActivity(), ProfileViewModelFactory(profileDAO))[ProfileViewModel::class.java]

        binding.tvEmailValue.setOnClickListener {
            Toast.makeText(requireContext(), "Update email not allowed!", Toast.LENGTH_SHORT).show()
        }

        toViewProfile()

        getProfile()

        binding.checkBtn.setOnClickListener {
            try {
                updateData(profile)
                Toast.makeText(requireContext(), "Profile Updated!", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_updateProfileFragment_to_profileFragment)
            } catch (e: Exception) {
                Toast.makeText(requireContext(), "Error: $e", Toast.LENGTH_SHORT).show()
            }
        }

        return(binding.root)
    }

    private fun toViewProfile() {
        binding.closeBtn.setOnClickListener {
            findNavController().navigate(R.id.action_updateProfileFragment_to_profileFragment)
        }
    }

    private fun getProfile() {
        profileViewModel.getUserByEmail()
        profileViewModel.profileLiveData.observe(viewLifecycleOwner){
            profile = it
            val editableEmail = Editable.Factory.getInstance().newEditable(it.email)
            val editableMobile = Editable.Factory.getInstance().newEditable(it.mobile)
            val editableAlamat = Editable.Factory.getInstance().newEditable(it.alamat)
            binding.tvEmailValue.text = editableEmail
            binding.tvMobileValue.text = editableMobile
            binding.tvAlamatValue.text = editableAlamat
        }
    }

    private fun updateData(profile: Profile) {
        val newAlamat = binding.tvAlamatValue.text.toString()
        val newMobile = binding.tvMobileValue.text.toString()

        profile.alamat = newAlamat
        profile.mobile = newMobile

        profileViewModel.updateProfileUser(profile)
    }
}